
import { Controller, Get } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Transport } from '@nestjs/microservices';
import {
  HealthCheckService,
  HealthCheck, 
  MicroserviceHealthIndicator 
} from '@nestjs/terminus';

@Controller('health')
export class HealthController {
  constructor(
    private health: HealthCheckService,
    private microservice: MicroserviceHealthIndicator,
    private config: ConfigService,
  ) {}


  @Get()
  @HealthCheck()
  apiCheck() {
    return this.health.check([
      () =>
      this.microservice.pingCheck('bookstore', {
        transport: Transport.TCP,
        options: {
          host: this.config.get<string>('BOOKSTORE_HOST'),
          port: this.config.get<number>('BOOKSTORE_PORT'),
        },
      }),

    ]);
  }
}



