import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CreateBookDto, UpdateBookDto } from './../common/dto/book.dto';
import { ApiOperation, ApiResponse, ApiTags} from '@nestjs/swagger';
import { BookService } from './book.service';


@ApiTags('books')
@Controller('books')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @ApiOperation({ summary: 'Добавить новую книгу' })
  @Post()
  async create(@Body() createBookDto: CreateBookDto) {
    return this.bookService.create({cmd: 'createBook'}, createBookDto);
  }

  @ApiOperation({ summary: 'Получить все книги' })
  @Get()
  async findAll() {
    return this.bookService.findAll({cmd: 'findAllBooks'});
  }

  @ApiOperation({ summary: 'Получить конкретную книгу' })
  @Get(':id')
  async findOne(@Param('id') id: string) {
    return this.bookService.findOne({cmd: 'findBook'}, id);
  }

  @ApiOperation({ summary: 'Обновить книгу' })
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateBookDto: UpdateBookDto) {
    return this.bookService.update({cmd: 'updateBook'}, id, updateBookDto);
  }

  @ApiOperation({ summary: 'Удалить книгу' })
  @Delete(':id')
  async remove(@Param('id') id: string) {
    return this.bookService.remove({cmd: 'removeBook'}, id);
  }


  @ApiOperation({ summary: 'Восстановить книгу' })
  @Delete(':id')
  async restore(@Param('id') id: string) {
    return this.bookService.restore({cmd: 'restoreBook'}, id);
  }
}
