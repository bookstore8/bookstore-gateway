import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateBookDto, UpdateBookDto } from './../common/dto/book.dto';
import { lastValueFrom, firstValueFrom, timeout } from 'rxjs';


@Injectable()
export class BookService {
    constructor(@Inject('BOOK_SERVICE') private client: ClientProxy) {}

    async create(cmd: Object, createBooData: CreateBookDto) {
        return await firstValueFrom(this.client.send(cmd, createBooData).pipe(timeout(5000)));
    }

    async findAll(cmd: Object) {
        return await lastValueFrom(this.client.send(cmd, {}).pipe(timeout(5000)))
    }

    async findOne(cmd: Object, id: string) {
        return await firstValueFrom(this.client.send(cmd, id).pipe(timeout(5000)))
    }

    async update(cmd: Object, id: string, updateBookData: UpdateBookDto) {
        return await firstValueFrom(this.client.send(cmd, { id: id, ...updateBookData }).pipe(timeout(5000)));
    }

    async remove(cmd: Object, id: string) {
        return await firstValueFrom(this.client.send(cmd, { id: id}).pipe(timeout(5000)));
    }


    async restore(cmd: Object, id: string) {
        return await firstValueFrom(this.client.send(cmd, { id: id}).pipe(timeout(5000)));
    }
}