import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, Transport,  } from '@nestjs/microservices';
import { AuthorController } from './author/author.controller';
import { AuthorService } from './author/author.service';

import { TerminusModule } from '@nestjs/terminus';
import { HealthModule } from './health/health.module'
import { BookController } from './book/book.controller';
import { GenreController } from './genre/genre.controller';

import { BookService } from './book/book.service';
import { genreService } from './genre/genre.service';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    ClientsModule.registerAsync([
      {
        name: 'BOOK_SERVICE',
        useFactory: async (configService: ConfigService) => ({
            transport: Transport.TCP,
            options: {
              host: configService.get<string>('BOOK_SERVICE_HOST'),
              port: configService.get<number>('BOOK_SERVICE_PORT'),
          },
        }),
        inject: [ConfigService, ]
      },
    ]),
    TerminusModule,
    HealthModule,
  ],
  controllers: [AuthorController, BookController, GenreController],
  providers: [
    AuthorService,
    BookService,
    genreService,
  ],
})
export class AppModule {}