import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateGenreDto } from './../common/dto/genre.dto';
import { lastValueFrom, firstValueFrom, timeout } from 'rxjs';


@Injectable()
export class genreService {
    constructor(@Inject('BOOK_SERVICE') private client: ClientProxy) {}

    async create(cmd: Object, createGenreData: CreateGenreDto) {
        return await firstValueFrom(this.client.send(cmd, createGenreData).pipe(timeout(5000)));
    }

    async findAll(cmd: Object) {
        return await lastValueFrom(this.client.send(cmd, {}).pipe(timeout(5000)))
    }

    async delete(cmd: Object, id: string) {
        return await firstValueFrom(this.client.send(cmd, { id: id}).pipe(timeout(5000)));
    }

    async restore(cmd: Object, id: string) {
        return await firstValueFrom(this.client.send(cmd, { id: id}).pipe(timeout(5000)));
    }
}