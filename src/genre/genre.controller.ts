import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CreateGenreDto } from './../common/dto/genre.dto';
import { ApiOperation, ApiTags} from '@nestjs/swagger';
import { genreService } from './genre.service';


@ApiTags('genres')
@Controller('genres')
export class GenreController {
  constructor(private readonly genreService: genreService) {}

  @ApiOperation({ summary: 'Добавить новый жанр' })
  @Post()
  async create(@Body() createGenreDto: CreateGenreDto) {
    return await this.genreService.create({cmd: 'createGenre'}, createGenreDto);
  }

  @ApiOperation({ summary: 'Получить список жанров' })
  @Get()
  async findAll() {
    return await this.genreService.findAll({cmd: 'findAllGenres'});
  }

  @ApiOperation({ summary: 'Удалить жанр' })
  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.genreService.delete({cmd: 'removeGenre'}, id);
  }

  @ApiOperation({ summary: 'Удалить жанр' })
  @Delete(':id')
  async restore(@Param('id') id: string) {
    return await this.genreService.restore({cmd: 'restoreGenre'}, id);
  }
}