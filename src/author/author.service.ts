import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { createAuthorDto, updateAuthorDto } from 'src/common/dto/author.dto';
import { lastValueFrom, firstValueFrom, timeout } from 'rxjs';
import { error } from 'console';


@Injectable()
export class AuthorService {
  constructor(@Inject('BOOK_SERVICE') private client: ClientProxy) {}

  async create(cmd: Object, createAuthorDto: createAuthorDto) {
    return await firstValueFrom(this.client.send(cmd, createAuthorDto).pipe(timeout(5000)));
  }

  async addBooksForAuthor(cmd: Object, booksForUpdate: { authorId: string, booksIds: Array<{id: string}>}) {
    return await firstValueFrom(this.client.send(cmd, booksForUpdate).pipe(timeout(5000)));
}


  async findAll(cmd: Object){
    return await lastValueFrom(this.client.send(cmd, {}).pipe(timeout(5000)))
  }

  async findOne(cmd: Object, id: string) {
    return await firstValueFrom(this.client.send(cmd, id).pipe(timeout(5000)))
  }

  async update(cmd: Object, id: string, author: updateAuthorDto) {
    return await firstValueFrom(this.client.send(cmd, {id:id, ...author}).pipe(timeout(5000)));
  }

  async changeRating(cmd: Object, gradeData: { id: string, grade: number }) {
    console.log(gradeData)
    try {
      this.client.emit(cmd, gradeData)
      return {statusCode: 201, message: 'рейтинг автора изменится в скором времени'}
    } catch (e) {
      console.log(`не удалось оценить автора: ${e}`)
      return {statusCode: 409, message: 'не удалось оценить автора'}
    }
  }
}