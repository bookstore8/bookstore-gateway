import { Controller, Get, Post, Inject, Body, Patch, Param, Put } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { createAuthorDto, updateAuthorDto, ReadAuthorDto, AuthorRateRequest} from './../common/dto/author.dto';
import { AuthorService } from './author.service';


@ApiTags('authors')
@Controller('authors')
export class AuthorController {
  constructor(private readonly authorService: AuthorService) {}

  @ApiOperation({ summary: 'Добавление нового автора' })
  @Post()
  async create(@Body() createAuthorData: createAuthorDto) {
    return await this.authorService.create({cmd: 'createAuthor'}, createAuthorData);
  }

  @ApiOperation({ summary: 'Получение списка всех авторов' })
  @ApiResponse({ status: 200, description: 'authors list.', type: Array<ReadAuthorDto> })
  @Get()
  async findAll(): Promise<Array<ReadAuthorDto>>{
    console.log(`SERACHING AUTHORS`)

    const authors = await this.authorService.findAll({ cmd: 'findAllAuthors' });
    console.log(`SERACHING AUTHORS: ${authors}`)
    return authors

  }

  @ApiOperation({ summary: 'Получение конкретного автора' })
  @ApiResponse({ status: 200, description: 'author.', type: ReadAuthorDto })
  @Get('/:id')
  async findOne(@Param('id') id: string): Promise<object> { 
    return await this.authorService.findOne({cmd: 'findAuthor'}, id);
  }

  @ApiOperation({ summary: 'Обновление автора' })
  @Patch('/:id')
  async update(@Param('id') id: string, @Body() author: updateAuthorDto) {
    return await this.authorService.update({ cmd: 'updateAuthor' }, id, author);
  }

  @ApiOperation({ summary: 'Обновление автора' })
  @Put('/:id')
  async addBooksForAuthor(@Param('id') id: string, @Body() updateData: { authorId: string, booksIds: Array<{id: string}>}) {
    return await this.authorService.addBooksForAuthor({ cmd: 'addBooksForAuthor' }, updateData);
  }

  @ApiOperation({ summary: 'Оценить автора, после оценки автора меняется его рейтинг' })
  @Post('/:id')
  async rateAuthor(@Param('id') id: string, @Body() gradeAuthorData: AuthorRateRequest ) {
    return await this.authorService.changeRating({cmd: 'rateAuthor'}, { id:id, grade: gradeAuthorData.grade });
  }
}