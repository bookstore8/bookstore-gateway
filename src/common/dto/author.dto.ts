import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import {
  IsNumber,
  IsPositive,
  Min,
  Max,
  IsString,
  IsDate,
  Length
} from 'class-validator';


export class baseAuthorDto {
  @ApiProperty({
    description: 'Имя автора'
  })
  @IsString()
  @Length(5, 50)
  firstName: string


  @ApiProperty({
    description: 'Фамилия автора'
  })
  @ApiPropertyOptional()
  @IsString()
  @Length(5, 100)
  lastName: string
}


export class createAuthorDto extends PartialType(baseAuthorDto) {
  @ApiProperty({
    description: 'Возраст автора'
  })
  @IsNumber()
  @IsPositive()
  @Min(15)
  @Max(150)
  age: number
}



export class updateAuthorDto extends PartialType(baseAuthorDto) {

}



export class ReadAuthorDto extends PartialType(baseAuthorDto) {
  @ApiProperty({
    description: 'id автора в формате uuid'
  })
  @IsString()
  id: string


  @ApiProperty({
    description: 'Рейтинг автора'
  })
  @IsNumber()
  @Min(0)
  @Max(5)
  rating: number


  @ApiProperty({
    description: 'Дата добавления автора'
  })
  @IsDate()
  createdAt: Date


  @ApiProperty({
    description: 'Дата изменения данных об авторе'
  })
  @IsDate()
  updatedAt: Date
}


export class AuthorRateRequest {
  @ApiProperty({
    description: 'Оценка, max - 5 min - 1'
  })
  @IsNumber()
  @Min(1)
  @Max(5)
  grade: number
}