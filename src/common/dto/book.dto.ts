import {
    IsNumber,
    IsString,
    IsDate
  } from 'class-validator';
import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';


export class ReadBookDto {
    @ApiProperty({
        description: 'Название книги'
    })
    @IsString()
    name: string


    @ApiProperty({
        description: 'рейтинг книги'
    })
    @IsNumber()
    rating: number

    @ApiProperty({
        description: 'Дата создания книги'
    })
    @IsDate()
    createdAt: Date

    @ApiProperty({
        description: 'Дата обновления книги'
    })
    @IsDate()
    updatedAt: Date

    @ApiProperty({
        description: 'Показывать/не показывать книгу'
    })
    isActive: boolean
}

export class CreateBookDto {
    @ApiProperty({
        description: 'название книги'
    })
    @IsString()
    name: string

    @ApiProperty({
        description: 'Сделать книгу активной'
    })
    @ApiPropertyOptional()
    isActive: boolean

    @ApiProperty({
        description: 'Жанр книги в формате {id: uuid}'
    })
    genre: {id: string}
}


export class UpdateBookDto extends PartialType(CreateBookDto){

}