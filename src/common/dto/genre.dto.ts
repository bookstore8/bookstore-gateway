import { PartialType, ApiProperty } from "@nestjs/swagger"
import {
    IsString,
} from 'class-validator';

export class CreateGenreDto {
    @ApiProperty({
        description: 'Название жанра'
    })
    @IsString()
    name: string
}

export class UpdateGenreDto extends PartialType(CreateGenreDto){
    @ApiProperty({
        description: 'Id жанра в формате uuid'
    })
    @IsString()
    id: string
}

export class ReadGenreDto {
    @ApiProperty({
        description: 'Id жанра в формате uuid'
    })
    @IsString()
    id: string

    @ApiProperty({
        description: 'Название жанра'
    })
    @IsString()
    name: string
}